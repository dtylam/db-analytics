package com.db.demomidtier;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Selvyn
 */
public class PropertyLoader {
    private Properties itsProperties = new Properties();
    static private PropertyLoader itsSelf = null;

    private PropertyLoader() {
    }

    static public PropertyLoader getLoader() {
        if (itsSelf == null)
            itsSelf = new PropertyLoader();
        return itsSelf;
    }

    public Properties getPropValues(String propFileName) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                itsProperties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (IOException e) {
            System.out.println("Exception: " + e);
            return null;
        }
        return itsProperties;
    }
}
