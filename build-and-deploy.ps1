﻿Param( [switch]$showhelp = $false,
       [string]$jarFile = "dbanalyzer-core.jar",
       [string]$warFile = "dbanalytics.war",
       [string]$destinationPath = "C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps"  );

function displayHelp
{
    Write-Host("`nusage: build-and-deploy.ps1 -jarfile [<jarfile name>] -warfile [<warfile name>] -destinationPath [<path>]" );
    Write-Host("`nusage: build-and-deploy.ps1 [<jarfile name>] [<warfile name>] [<path>]" );
    Write-Host("`nother optons"); 
    Write-Host("`t-help       `tdisplay this usage message"); 
    Write-Host("`nNamed parameters example:"); 
    Write-Host("`========================"); 
    Write-Host("build-and-deploy.ps1 -jarfile dbanalyzer-core.jar -warFile dbanalytics.war -destinationPath C:\java\apache-tomcat-9.0.7\webapps`n`n");
    Write-Host("Positional parameters example:"); 
    Write-Host("============================="); 
    Write-Host("build-and-deploy.ps1 dbanalyzer-core.jar dbanalytics.war C:\java\apache-tomcat-9.0.7\webapps`n`n");
}


function copyFileIfExist
{
    param( [parameter(Mandatory=$true)][String]$sourcefile, 
            [parameter(Mandatory=$true)][String]$destinationPath );

    $result = $false;
    $location = Get-Location;
    $location;
    $fileToCopy = $location.Path + "\" + $sourcefile;

    if( [System.IO.File]::Exists($fileToCopy) )
    {
        $result = $true;
        Write-Host( $fileToCopy + " exists" );
        Copy-Item $fileToCopy -Destination $destinationPath;
    }
    else
    {
        Write-Host $testFile + " does NOT exist";
    }
    return $result;
}

function buildApplications
{
    param( [parameter(Mandatory=$true)][String]$warfile, 
            [parameter(Mandatory=$true)][String]$jarFile,
            [parameter(Mandatory=$true)][String]$destinationPath );

    cd dbanalyzer-core
    mvn clean install

    $location = Get-Location;
    $jarFileWithPath = $location.Path + "\target\" + $jarfile;

    #if( [System.IO.File]::Exists($location.Path + "\target\dbanalyzer-core.jar") )
    if( [System.IO.File]::Exists($jarFileWithPath) )
    {
        cd ..\dbanalyzer-front
        mvn clean package

        #copyFileIfExist -sourceFile "target\dbanalytics.war" -destinationPath "C:\java\apache-tomcat-9.0.7\webapps";
        copyFileIfExist -sourceFile ("target\"+$warfile) -destinationPath $destinationPath;

        cd ..
    }
}

if( $showhelp )
{
    displayHelp;
}
else
{
    buildApplications -jarfile $jarFile -warfile $warFile -destinationPath $destinationPath;


    #buildApplications -jarfile "dbanalyzer-core.jar" -warfile "dbanalytics.war" -destinationPath "C:\java\apache-tomcat-9.0.7\webapps";
}