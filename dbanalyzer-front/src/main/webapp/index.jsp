<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="theController" class="com.db.demomidtier.LoginController" scope="application"/>
 
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Donald + Niki's DB Apollo 13</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://d3js.org/d3.v4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="navControls.js"></script>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

   
  </head>

  <body>
    <div class="page-signin text-center">
      <form id="loginForm" class="form-signin">
      <img class="mb-4" src="db-analytics.png" alt="" width="250">
      <h3>Connected: <%
      if (theController.isConnected())
      {
      %>
       <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAYnSURBVGhD1ZoHiB1VFIbXkmg0xihGUaOGmNgbVhSCYI2N2HuNYjcoVjQRC4oNQU3E2MGCBUWxgA0LdjQQJYqJmNhb7N3E8n2ze5a7b9/bN+ObV/zhY3dmp9x67n/PbFdBLQobweFwIdwFL8P78Al8C3/D7z2/z4V34Gm4Ac6EvWBlaLlWgVPgYfgG/imJ2XAz7AtLQFO0CNhyj8FCqCzAnXA+HAxbwWgYAUNBef9yYCOMhe3hOLgSHgF7K32mDXQdbAClaTt4E+Ilv4IF3x/KGhIO0Q1hEjg0411/wa2wOvxnLQN3Qzx0DtiKy0KztQ5cDjZaNN6xUFgOixngQ74H58RgaLVWhdsgGtOhmFt283PgjY5/W6fdOhKMfJbpZE/kkTd5wxewkic6RAeC5foJVvBEPcVkOzQ76iw9BJbtxOyojn4BL14+O+osufBatpuyozryQhmeHXWWjJqWTQdRV1GR07KjzpFB6EUoXBGH2Pqe6BCdDVG2QhURrcOO0E4tBpdBWq5CFfFif+qtpkE7HOo28ApYjt8gylSoIuoM+AM8Nn5fDXqiZmoQTADNZJRlHmwJu/UcF66IWhP0XO4r4m8zwf2HLnZpaFRaEZ3zdPga4j3fgXNjCKiGKhJaD66CTyGukT9Bd3wvXAJHw+6ga94MtDhuvrT3O8DeoMu9Fh6HDyB9nrwOWhGtf6pSKhJy8hkALgXDYQy9RpgPrtqnwtpQS6VWpFJ2+6bg/uRccP/wIDwFr4LD8A14AZ6A+8De0FHvCmuBG688sqcLVySXMWux9FiFKzI5O+ocGc1ip1qoIo79rT3RIUoXxUIVEW2KY7+dMqNidiUtV6GKXNPz0/XjfhgDrdaeMAssxw+gwyhcEXUE/AgeL4A7wLVAJ9osmdxwLUqzKW+D61FD4dfoZbhM14uP4Hpw+2muqhHZKJuAi+QDEJkTMVs5ERYHVco6YoG1C+9CXBN8CKZBb4Sz4CjYD3aGcaCVcQ1wvrk5uhgszGtgliZ9libVROABsCSkMrvzKPisuooHDqSNwY1XWWnT98Dt62FQmsuOh+eVQ2MNsNVNoplY0/zpvXSwrvDisDFDafJ6CmgSdbR5cgPm1Mw2bg7rgsnDuoqKtCVDXiGHphHTLUSUS0ylan8cFeGM+ykutmXbpZHwLKQFN2X7EhjB0op9BobpfooLnHS7eKLF0vp/DlFIo9mKkMqhZtkiI+paZyDqo6iIuHacBK2SPRGVMJDkmQvHg3si7zFL2quohKbRLvV3u9mJ2Ww9A1EJ9z15ZeCIL2OjPKGiImoPsHs99kJ3dMb3mhOsAY0H32OP5IpKyDXrhO5fszXM+3UfmdKKKL862TthVcRFzPDqdtRkRBmWxejks50TeXQI2LjOZUOyUVb3Ya8Mg34VCemBHIPPgw+I6+RncCfoWmG0s6VcU0yEu8IHx8B5oCH1OORew0jkUK6c2NXk/j8+M0SPKHegntvHgyjkQHJxMqGsvTYsRoWK8BaEfJ7n/BqcKjxWKl34V+D1pqdSmdnxvFvu3jmxmgc55fAzU2KGxE/OboKmwi3gHl1uB234BWAr+lE0ZAv7Th1vyCFrI22bHXVLAxsNVy0gGGH9myY3sxEe+NJWycyJ70x7SZvjOeeAlkYDGVFNs7kUVCryw5rSrGW92SFmhGqFnJzODwNKfGO3tS+CWAJiffkYXG+qyQyO1+iwM50OnrAytkYzN1IhvZPvNAyn2gliThgta6VsnU9fgteZHe2VkSdawy+8lS8oWxpA32X+q1KmVN3vDGSZXOG930xLP5lRTKOSmXFzS834LOeYj3RsGlLzyP+0iJyxu8iqssssfIxRMYbbQv7bhlGl2sTLK11CRB+z8A5nF7aDPJFDViISFC7SdRXpfnO0YdICX+4+Xk/m2mL4PQdcpV0YxaFqnDcRfg84J/wE7v2G55DXxTO1HbX2RTawwyl6wucVblCjjF14BTjc/AgTlSqKrsDolCr9BwF750mI9ccQa3SKiS02RBmfN7IktBPSYWYhfJkvtqKuBy6O0Uv6M8P6FmAyoZbcPrumRYWqYRAyEf6/kE5Y76RH81+f7D3XiT4htq+6uv4Fp6FWsqXOArEAAAAASUVORK5CYII=">
       <%
       } else {
       %>
       <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAXhSURBVGhD1ZoHiB1VFIY3GnsXC3bR2BIbVhREsGDHEjvG2LArESs2LCg2BFuwJ6CRRIkoFrBhITFRNBBFxYINK2rsvX7fsGe5+zZv39z35hV/+MjOZMqt5/73zOvL1AKwKRwJl8IUeBHeg09gHvwDv/X//QG8CU/DrXA27A+rQMe1KpwGD8M38G9FvAN3woGwCLRFI8CWewz+gtoC3AsXw+GwLawDK8KSoLx/ObAR1oOd4QS4Fh4Beyt9pg10E2wMlWkneBXiJb+ABT8YqhoSDtFN4HRwaMa7/oa7YU1oWkvBfRAPfRdsxWWg3doQrgYbLRrveMiWw2IO+JDvwDmxMHRaq8EkiMZ0KJaW3fwceKPj39bpto4CI59lOtUTZeRN3vAFrOyJHtGhYLl+hBU80Ugx2Y4ojnpLD4FlO7k4aqCfwYuXL456Sy68lu2O4qiBvFCWLY56S0ZNy6aDaKioyBnFUe/IIDQDsiviEBvjiR7RuRBly6qIaB12hW5qQbgK0nJlVcSL/VdvdQt0w6FuD7PAcvwKUaasiqiz4Hfw2Ph9PeiJ2qmFYF/QTEZZPoRtYK/+4+yKqHVBz+W+Iv5vLrj/0MUuAa1KK6Jzvg2+gnjPt+DcWAxUSxUJjYbr4FOIa+QP0B1PgyvgWNgbdM1bghbHzZf2fhc4AHS5N8Lj8D6kz5OXQSui9U9VSUVCTj4DwJVgOIyh1wpfg6v2BNgA6qnSitTKbt8C3J+cD+4fHoSnYDY4DF+BF+AJuB/sDR31nrA+uPEqI3s6uyKljFmHpcfKrsiFxVHvyGgWO9Wsijj2t/NEjyhdFLMqItoUx343ZUbF7EparqyK3ND/r+vHAzAKOq394A2wHN+DDiO7Imo8/AAe/wn3gGuBTrRdMrnhWpRmU14H16OWwq/Ry3CZrhcfw0Rw+2muqhXZKJuDi+R0iMyJmK08BkaCqmQdscDahbcgrgk+AtOgt8M5cDQcBLvBDqCVcQ1wvrk5uhwszEtgliZ9libVROAhsCikMrvzKPishooHDqfNwI1XVWnTt8Ht6ziozGXHw8vKobEW2Oom0Uysaf70XjpYV3hx2JihNHl9EWgSdbRlcgPm1Mw2bgUbgcnDhoqKdCVDXiOHphHTLUSUS0ylan8cFeGMhygutmW7pdXhWUgLbsp2JhjB0op9BobpIYoLnHR7eKLD0vp/DlFIo9lKkMqhZtkiI+paZyAapKiIuHacAp2SPRGVMJCUmQsngnsi7zFLOqCohKbRLvVvu9mJ2W49A1EJ9z1lZeCIL2Nre0JFRdQ+YPd67IXu6IzvdSdYC9odfI89Uioq1cg1zPt1H4XSiii/Otk7YVXERczw6nbUZEQVlsXo5LOdE83IKKv7sFeW9kRtRUJ6IMfg85AmIuQncCfoWmG0c3V3TTER7gofHAcXgIbU45B7DSORQ7l2YufIHajlGetBFHI4uTiZUNZeGxbTSpXlNQj5PM/5NThkC6eNUA+TGiEzOz7HLffAnFjDg5Jy+JkpMUPiJ2c3QTfDXeAeXSaDNvwSOAn8KBqyML5TxxsKg9iI1EAaYT2nyS1shAe+tFMyc+I7014yoRGNMBzpnIr8sKa0aFkXQ4eYEaoTcnI6PwworXxjN4NjRXTYhc4ET1gZDV47N1IhvZPvNAw3I/csX4LPMDs6ICNPLIh+4W32BWWlAfRd5r+akSu895tpGSIzimlUMjNubqkdn+UWh0jHGgxy5C8tImdskJiv7DILHx5IXHTcEfqzjR3BQjQrXULYEbPwDmcXtsM8UUJWIhIULtINFel+c7Rh0gJf7j5eT+baYvg9D4woLoziUDXOmwifCs4JP4F7v9En5HXxTG1HvX2RDexwip7wedkNapSxC68Bh5sfYaJSuegKLoNU6Q8E7J0nIdYfQ6zRKSa22BBVfN4oktB+43CYWQhf5outqNteF8foJf2ZYX1rMJlQT26fXdOiQvPDIGQi/H8hnbDeSY/mT5/sPdeJQSF2sPr6/gNRm06K+GZ1egAAAABJRU5ErkJggg==">
       <% } %>
       </h1>
      <div id="slideUpDiv">
      <h2 class="h3 mb-3 font-weight-normal">Please sign in</h2>
      <label for="inputUsername" class="sr-only">Username</label>
      <input type="text" name="inputUsername" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </div>
      <button class="btn btn-lg btn-info btn-block" id="redirectButton" type="button">View Data</button>
      <p id="loginDetails"></p>

      <p class="mt-5 mb-3 text-muted">&copy; celestial consulting 2018</p>
    </form>
    </div>
    <div id="content" style="display:none"></div>
    
      
     <script>
        $("#loginForm").submit(function(e) {
        e.preventDefault();
        var userName = $("#inputUsername").val();
        var userPassword = $("#inputPassword").val();
        console.log("function called");
        $.ajax({
          method: 'POST',
          url: "loginCheck.jsp",
          data: "user="+userName+"&pass="+userPassword,
          success: function(msg) {
            if(msg == "error") {
              alert("Invalid username or password");
            } else {
              var loginObj = JSON.parse(msg);
              $("#loginDetails").html("Successful login: " + msg);
              $("#slideUpDiv").slideUp(500);
              $("#redirectButton").slideDown(500);
            }
          },
          error: function(msg) {
            console.log("didnt work");
          }
        });
        return false;
      });
        $("#redirectButton").click(function() {
          $.ajax({
            method: 'POST',
            url: "dashboard.jsp",
            data: "",
            success: function(msg) {
              $(".page-signin").slideUp(500);
              $("#content").html(msg);
              $("#content").slideDown(500);
            },
            error: function(msg) {
              console.log(msg);
            }
          });
          return false;
        });
    </script>
  </body>
</html>