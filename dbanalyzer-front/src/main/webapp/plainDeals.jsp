<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Map" %>
<jsp:useBean id="userDataController" class="com.db.demomidtier.UserData" scope="application"/>
<%
    String[] columnNames = new String[] {"deal_id", "deal_amount", "deal_quantity",
            "counterparty_name", "instrument_name", "deal_type", "deal_time"};

    try{
        JSONObject result = userDataController.getDealsTable(
                Integer.valueOf(request.getParameter("draw")),
                Integer.valueOf(request.getParameter("start")),
                Integer.valueOf(request.getParameter("length")),
                columnNames[Integer.valueOf(request.getParameter("order[0][column]"))],
                (request.getParameter("order[0][dir]").equals("asc"))
        );

        if(result != null && !result.has("error")){

            JSONObject table = new JSONObject();

            for (String key : new String[]{"draw", "recordsTotal", "recordsFiltered"}) {
                if (result.has(key)) {
                    table.put(key, result.get(key));
                }
            }

            JSONArray rows = new JSONArray();
            if (result.has("data")) {
                for (Object obj : (JSONArray) result.get("data")) {
                    JSONObject jo = (JSONObject) obj;

                    JSONArray innerArray = new JSONArray();
                    for (String column : columnNames) {
                        innerArray.put(jo.get(column));
                    }
                    rows.put(innerArray);
                }
            }
            table.put("data", rows);
            out.print(table);
        } else {
            out.print("error");
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
%>